import sys
from Node import Node
  
def unival(node):
    return univalHelper(node)[0]

def univalHelper(node):
    if node.left == None and node.right == None:
        return (1, True)
    
    numberOfUnivals = 0
    childrenAreUnivals = True
    if node.left != None:
        leftUnivalTuple = univalHelper(node.left)
        numberOfUnivals += leftUnivalTuple[0]
        childrenAreUnivals = childrenAreUnivals and leftUnivalTuple[1]
    if(node.right != None):
        rightUnivalTuple = univalHelper(node.right)
        numberOfUnivals += rightUnivalTuple[0]
        childrenAreUnivals = childrenAreUnivals and rightUnivalTuple
    
    if childrenAreUnivals and node.val == node.left.val and node.val == node.right.val:
        return (numberOfUnivals + 1, True)

    return (numberOfUnivals, False)

if __name__ == '__main__':
    print('No main for this file')
