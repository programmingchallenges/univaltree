import unittest
import main
from Node import Node

class TestStringMethods(unittest.TestCase):

    def testBasic(self):
        node = Node('root')
        self.assertEqual(main.unival(node), 1)

    def testExample(self):
        node = Node('0', Node('1'), Node('0', Node('1', Node('1'), Node('1')), Node('0')))
        self.assertEqual(main.unival(node), 5)

    def testOnlyLeafUnivals(self):
        node = Node('0', Node('1'), Node('0', Node('0', Node('1'), Node('1')), Node('0')))
        self.assertEqual(main.unival(node), 4)

if __name__ == '__main__':
    unittest.main()